import { Component, OnInit } from '@angular/core';

import { Sensor } from "../sensor";
import { SensorService } from "../sensor.service";

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit{
  AllSensors: Sensor[] = [];
  AllSensorList: string[] = [];

  constructor(private SensorService: SensorService) { }

  ngOnInit(): void {
    this.SetSensors();
    
  }

  SetAllSensors(): void {
    this.AllSensorList = this.SensorService.getSensorsList();
  }
  /*----------------------------*/
  SetSensors(): void {
    this.AllSensors = this.SensorService.getSensors();
  }

}
