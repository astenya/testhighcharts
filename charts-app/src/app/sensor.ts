import { SENSORTYPE } from "./sensor-data";

export class Sensor {
  id: number;
  name: string;
  sensorType: SENSORTYPE;
  data: { sensorTime: string; sensorValue: number }[];
  information: { longitude: number; latitude: number; elevation: number };

  constructor(
    id: number,
    name: string,
    sensorType: SENSORTYPE,
    data: { sensorTime: string; sensorValue: number }[]
  ) {
    this.id = id;
    this.name = name;
    this.sensorType = sensorType;
    this.data = data;
  }
}
