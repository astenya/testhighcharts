import { Component, OnInit, AfterViewChecked, ViewEncapsulation } from "@angular/core";

import { Sensor } from "../sensor";
import { SensorService } from "../sensor.service";
import * as Highcharts from "highcharts";
import { SENSORTYPE, SENSORS } from "../sensor-data";
//import { FormsModule } from "@angular/forms";
//import { MatFormFieldModule } from '@angular/material/form-field';


@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, AfterViewChecked {
  
  AllSensors: Sensor[] = [];  
  AllSensorList: string[] = [];
  AllSensorTypes: SENSORTYPE[] = [];
  WorkSensorList: Sensor[] = [];
  curType: string[] = ["line", "line", "line"];
  curTypeUnion: string= "line";
  listChartTypes: string[] = ["line", "bar", "spline"];
  defaulDateStart = new Date("2020-03-01T00:00:00.517Z");
  defaulDateEnd = new Date("2020-03-02T00:00:00.517Z");
  curSensorCheckBox: boolean [] = [];

  highcharts = Highcharts;

  wasViewLoadedOnce: boolean = false;

  constructor(private SensorService: SensorService) {}

  ngOnInit(): void {
    this.createData();
    this.SetAllSensors();
    this.SetSensors();
    this.SetSensorType();
    this.SetCurSensorCheckBox();    

  }

  ngAfterViewChecked(): void {
    if (!this.wasViewLoadedOnce) {
      this.MakeChart(true);
      this.wasViewLoadedOnce = true;
    }
  }
  createData(): void{
    this.SensorService.createSensorData(this.defaulDateStart, this.defaulDateEnd);
  }

  SetSensors(): void {
    this.AllSensors = this.SensorService.getSensors();
  }
  SetAllSensors(): void {
    this.AllSensorList = this.SensorService.getSensorsList();
  }
  SetCurSensorCheckBox():void{
    for(let i=0; i<this.AllSensorList.length;i++){
      this.curSensorCheckBox[i] = true;
    }
  }

  SetSensorType(): void {
    this.AllSensorTypes = this.SensorService.getSensorType();
  }


  createChartOptions(curSensorType: SENSORTYPE): any {
    let curChartOptions: any = [];

    switch (curSensorType) {
      case SENSORTYPE.temperature: {
        curChartOptions = {
          chart: {
            renderTo: "temperatureChart",
            type: "spline"
          },
          title: {
            text: "Показатели температуры."
          },
          xAxis: {
            title: {
              text: "Время "
            },
            type: "datetime"
          },
          yAxis: {
            title: {
              text: "Temperature °C"
            },
            tickInterval: 10
          },
          tooltip: {
            valueSuffix: " °C"
          },
          series: []
        };
        break;
      }
      case SENSORTYPE.humidity: {
        curChartOptions = {
          chart: {
            renderTo: "humidityChart",
            type: "spline"
          },
          title: {
            text: "Показатели влажности."
          },
          xAxis: {
            title: {
              text: "Время "
            },
            type: "datetime"
          },
          yAxis: {
            title: {
              text: "Влажность %"
            },
            tickInterval: 10
          },
          tooltip: {
            valueSuffix: " %"
          },
          series: []
        };
        break;
      }
      case SENSORTYPE.light: {
        curChartOptions = {
          chart: {
            renderTo: "lightChart",
            type: "spline"
          },
          title: {
            text: "Показатели освещенности."
          },
          xAxis: {
            title: {
              text: "Время "
            },
            type: "datetime"
          },
          yAxis: {
            title: {
              text: "Освещенность lm"
            },
            tickInterval: 10
          },
          tooltip: {
            valueSuffix: " lm"
          },
          series: []
        };
        break;
      }
      default: {
        break;
      }
    }
    return curChartOptions;
  }

  createCurSensorList(): string[] {
    let list: string[] = [];

    for (let i = 0; i < this.AllSensorList.length; i++) {
      if(this.curSensorCheckBox[i]===true){
        list.push(this.AllSensorList[i]);
      }
      /*if (
        (<HTMLInputElement>document.getElementById("sensor" + i)).checked ===true
      ) {
        list.push(this.AllSensorList[i]);
      }*/
    }
    return list;
  }

  MakeChart(isInit: boolean): void {
    Highcharts.setOptions({
      title: {
        style: {
          color: "orange"
        }
      }
    });

    let unionChartOptions: any = {
      chart: {
        renderTo: "chartsBlock",
        type: "spline"
      },
      title: {
        text: "Объединеные данные."
      },
      xAxis: {
        title: {
          text: "Время "
        },
        type: "datetime"
      },
      yAxis: {
        title: {
          text: ""
        },
        tickInterval: 10
      },
      tooltip: {
        valueSuffix: " "
      },
      series: []
    };

    let needSensorList: string[];
    let needDateStart = this.defaulDateStart;
    let needDateEnd = this.defaulDateEnd;

    if (isInit) {
      needSensorList = this.AllSensorList;
    } else {
      needSensorList = this.createCurSensorList();
      needDateStart = new Date(this.defaulDateStart);
      needDateEnd = new Date(this.defaulDateEnd);
    }

    this.AllSensorTypes.forEach(st => {
      let needSensorTypes: SENSORTYPE[] = [];
      needSensorTypes.push(st);

      let curWorkSensorList: Sensor[] = [];
      curWorkSensorList = this.SensorService.getSensorsFromListNameByTypeAndDatas(
        needSensorList,
        needSensorTypes,
        needDateStart,
        needDateEnd
      );
      
      let options: any = this.createChartOptions(st);
      options.chart.type = this.curType[st];

      curWorkSensorList.forEach(function(ell) {
        options.series.push({
          name: ell.name,
          data: ell.data.map(function(d) {
            let point = [];
            point.push(new Date(d.sensorTime).getTime(), d.sensorValue);
            return point;
          })
        });
      });

      /*if (isInit){
        let place = <HTMLElement>document.getElementById("chartsBlock");
        let chartBlock = <HTMLElement>document.createElement("div");
        chartBlock.id = "chartBlock_" + options.chart.renderTo;
        chartBlock.className = 'margin1';
        //chartBlock.setAttribute("style", "margin-top: 1%");
        place.appendChild(chartBlock);
        let optionBlock = <HTMLElement>document.createElement("div");
        optionBlock.id = "optionTo_" + options.chart.renderTo;
        optionBlock.innerHTML = `
          <span>Выберите тип графика: </span>
          <select [(ngModel)]="this.curType"  (ngModelChange)="MakeChart(false)">
            <option>line </option>
            <option>Bar</option>
          </select>`;
        chartBlock.appendChild(optionBlock);
        let chart = <HTMLElement>document.createElement("div");
        chart.id = "chart" + options.chart.renderTo;
        chartBlock.appendChild(chart);
      }*/

      
      let idNeedBlock = "chart" + st;
      
      let curChart = new Highcharts.Chart(idNeedBlock, options);
    });

    let curWorkSensorList: Sensor[] = [];

    curWorkSensorList = this.SensorService.getSensorsFromListNameByTypeAndDatas(
      needSensorList,
      this.AllSensorTypes,
      needDateStart,
      needDateEnd
    );

    curWorkSensorList.forEach(function(ell) {
      unionChartOptions.series.push({
        name: ell.name,
        data: ell.data.map(function(d) {
          let point = [];
          point.push(new Date(d.sensorTime).getTime(), d.sensorValue);
          return point;
        })
      });
    });

    unionChartOptions.chart.type = this.curTypeUnion;

    let unionChart = new Highcharts.Chart("union", unionChartOptions);
  }  

  UpdateSensorsData(): void {
    this.SensorService.createSensorData(new Date(this.defaulDateStart), new Date(this.defaulDateEnd));
    this.MakeChart(false);
  }
}
