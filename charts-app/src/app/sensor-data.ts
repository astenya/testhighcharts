import { Sensor } from './sensor';

export const enum SENSORTYPE{
    temperature,
    humidity,
    light
}

export const SENSORS: Sensor[] = [
    {
        id: 1,
        name: 'датчик 1',
        sensorType: SENSORTYPE.temperature,
        data: [],
        information:{
            longitude: 28.356432,
            latitude: 31.678873,
            elevation: 560
        }
    },
    {
        id: 2,
        name: 'датчик 2',
        sensorType: SENSORTYPE.humidity,
        data: [],
        information: {
            longitude: 45.356432,
            latitude: 72.678873,
            elevation: 300
        }
    },
    {
        id: 3,
        name: 'датчик 3',
        sensorType: SENSORTYPE.light,
        data: [],
        information: {
            longitude: 68.356432,
            latitude: 54.678873,
            elevation: 1500
        }
    },
    {
        id: 4,
        name: 'датчик 4',
        sensorType: SENSORTYPE.temperature,
        data: [],
        information: {
            longitude: 67.356432,
            latitude: 86.678873,
            elevation: 790
        }
    },
    {
        id: 5,
        name: 'датчик 5',
        sensorType: SENSORTYPE.temperature,
        data: [],
        information: {
            longitude: 59.356432,
            latitude: 72.678873,
            elevation: 1300
        }
    },
    {
        id: 6,
        name: 'датчик 6',
        sensorType: SENSORTYPE.humidity,
        data: [],
        information: {
            longitude: 46.356432,
            latitude: 85.678873,
            elevation: 1100
        }
    },
    {
        id: 7,
        name: 'датчик 7',
        sensorType: SENSORTYPE.light,
        data: [],
        information: {
            longitude:775.356432,
            latitude: 89.678873,
            elevation: 1200
        }
    },
    
];