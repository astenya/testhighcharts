import { Injectable } from "@angular/core";

import { Sensor } from "./sensor";
import { SENSORS } from "./sensor-data";
import { SENSORTYPE } from "./sensor-data";

@Injectable()
export class SensorService {
  constructor() {}

  getSensorType(): SENSORTYPE[] {
    let listSensorType: SENSORTYPE[] = [];
    listSensorType.push(
      SENSORTYPE.humidity,
      SENSORTYPE.light,
      SENSORTYPE.temperature
    );
    return listSensorType;
  }
  
  getSensors(): Sensor[] {
    return SENSORS;
  }

  getSensorsList(): string[] {
    return SENSORS.map(function(s) {
      return s.name;
    });
  }

  getSensorByName(name: string): Sensor {
    return SENSORS.find(sensor => sensor.name === name);
  }
  getSensorsFromListName(listName: string[]): Sensor[] {
    return SENSORS.filter(sensor => listName.includes(sensor.name) === true);
  }

  getSensorsFromListNameByTypeAndDatas(
    listName: string[],
    listSensorType: SENSORTYPE[],
    startDate: Date,
    endDate: Date
  ): Sensor[] {
    return SENSORS.filter(
      sensor =>
        listName.includes(sensor.name) &&
        listSensorType.includes(sensor.sensorType) &&
        sensor.data.some(
          row =>
            new Date(row.sensorTime) >= startDate &&
            new Date(row.sensorTime) <= endDate
        )
    ).map(function(s) {
      return new Sensor(
        s.id,
        s.name,
        s.sensorType,
        s.data.filter(
          row =>
            new Date(row.sensorTime) >= startDate &&
            new Date(row.sensorTime) <= endDate
        )
      );
    });
  }

  getSensorByType(needType: SENSORTYPE): Sensor {
    return SENSORS.find(sensor => sensor.sensorType === needType);
  }

  getSensorByDate(startDate: Date, endDate: Date): Sensor[] {
    return SENSORS.filter(x =>
      x.data.some(
        row => new Date(row.sensorTime) >= startDate && new Date(row.sensorTime)
      )
    ).map(function(s) {
      return new Sensor(
        s.id,
        s.name,
        s.sensorType,
        s.data.filter(
          row =>
            new Date(row.sensorTime) >= startDate && new Date(row.sensorTime)
        )
      );
    });
  }

  createSensorData(dateStart: Date, dateEnd: Date): void {
    SENSORS.forEach(element => {
      element.data = [];

      let leftLimit = 0;
      let rightLimit = 100;

      switch (element.sensorType) {
        case SENSORTYPE.temperature: {
          leftLimit = 0;
          rightLimit = 50;
          break;
        }
        case SENSORTYPE.humidity: {
          leftLimit = 30;
          rightLimit = 98;
          break;
        }
        case SENSORTYPE.light: {
          leftLimit = 0;
          rightLimit = 100;
          break;
        }
        default: {
          break;
        }
      }
      
      let curDate = new Date(dateStart);
      curDate.setUTCHours(0);

      let dateTo = new Date(dateEnd);
      dateTo.setUTCHours(23);
      dateTo.setUTCMinutes(59);
      
      //alert("curDate = " + curDate.toISOString());
      
      while (curDate <= dateTo) {
        
        for (let j = 0; j < 24; j++) {
          
          curDate.setUTCHours(j);          

          element.data.push({
            sensorTime: curDate.toISOString(),
            sensorValue: Math.floor(Math.random() * rightLimit + leftLimit)
          }); 
          //alert(curDate.toISOString());         
        }
        curDate.setUTCDate(curDate.getUTCDate() + 1);
      }
    });
  }
}
