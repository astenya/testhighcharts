import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SensorService } from "./sensor.service";
import { HomeComponent } from "./home/home.component";
import { FormsModule } from '@angular/forms';
import { ChartsConstructorComponent } from './ChartsConstructor/ChartsConstructor.component';
import { InformationComponent } from './information/information.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select'


//import {MatLabelModule} from '@angular/material/label';

//import { HighchartsChartComponent } from "highcharts-angular";

@NgModule({
  declarations: [AppComponent, HomeComponent, ChartsConstructorComponent, InformationComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule, BrowserAnimationsModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatInputModule, MatSelectModule],
  providers: [SensorService, MatDatepickerModule],
  bootstrap: [AppComponent]
})
export class AppModule {}
