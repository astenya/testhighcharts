import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartsConstructorComponent } from './ChartsConstructor.component';

describe('StartComponent', () => {
  let component: ChartsConstructorComponent;
  let fixture: ComponentFixture<ChartsConstructorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartsConstructorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartsConstructorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
